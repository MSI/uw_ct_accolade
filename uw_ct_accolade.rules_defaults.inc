<?php
/**
 * @file
 * uw_ct_accolade.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_ct_accolade_default_rules_configuration() {
  $items = array();
  $items['rules_accolade_change_status'] = entity_import('rules_config', '{ "rules_accolade_change_status" : {
      "LABEL" : "accolade change status (approved)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "accolade" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "accolade" : "accolade" } } } },
        { "NOT data_is" : { "data" : [ "node-unchanged:field-status" ], "value" : "approved" } },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_publish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_accolade_change_status_not_approved_'] = entity_import('rules_config', '{ "rules_accolade_change_status_not_approved_" : {
      "LABEL" : "accolade change status (not approved)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "accolade" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "accolade" : "accolade" } } } },
        { "data_is" : { "data" : [ "node-unchanged:field-status" ], "value" : "approved" } },
        { "NOT data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_unpublish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_accolade_initial_submit_approved'] = entity_import('rules_config', '{ "rules_accolade_initial_submit_approved" : {
      "LABEL" : "accolade initial submit approved",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "accolade" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "accolade" : "accolade" } } } },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_publish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_accolade_initial_submit_email'] = entity_import('rules_config', '{ "rules_accolade_initial_submit_email" : {
      "LABEL" : "accolade initial submit email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "accolade" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "accolade" : "accolade" } } } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "6" : "6", "5" : "5" } },
            "subject" : "A new accolade has been submitted for review",
            "message" : "We have received a accolade submission to [site:name]. Please login and review the submission."
          }
        }
      ]
    }
  }');
  $items['rules_accolade_published'] = entity_import('rules_config', '{ "rules_accolade_published" : {
      "LABEL" : "accolade published",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "TAGS" : [ "accolade" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "accolade" : "accolade" } } } },
        { "node_is_published" : { "node" : [ "node" ] } },
        { "NOT data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ]
    }
  }');
  $items['rules_accolade_submit'] = entity_import('rules_config', '{ "rules_accolade_submit" : {
      "LABEL" : "accolade initial submit",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "accolade" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "accolade" : "accolade" } } } }
      ],
      "DO" : [ { "redirect" : { "url" : "accolade-submitted" } } ]
    }
  }');
  $items['rules_accolade_unpublished'] = entity_import('rules_config', '{ "rules_accolade_unpublished" : {
      "LABEL" : "accolade unpublished",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "TAGS" : [ "accolade" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "accolade" : "accolade" } } } },
        { "NOT node_is_published" : { "node" : [ "node" ] } },
        { "NOT data_is" : {
            "data" : [ "node:field-status" ],
            "op" : "IN",
            "value" : { "value" : { "decision pending" : "decision pending", "declined" : "declined" } }
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-status" ], "value" : "declined" } }
      ]
    }
  }');
  return $items;
}
