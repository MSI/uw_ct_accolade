<?php
/**
 * @file
 * uw_ct_accolade.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_accolade_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_accolade_type'.
  $permissions['create field_accolade_type'] = array(
    'name' => 'create field_accolade_type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_accolade_type_other'.
  $permissions['create field_accolade_type_other'] = array(
    'name' => 'create field_accolade_type_other',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_introduction_text'.
  $permissions['create field_introduction_text'] = array(
    'name' => 'create field_introduction_text',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_status'.
  $permissions['create field_status'] = array(
    'name' => 'create field_status',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create accolade content'.
  $permissions['create accolade content'] = array(
    'name' => 'create accolade content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any accolade content'.
  $permissions['delete any accolade content'] = array(
    'name' => 'delete any accolade content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own accolade content'.
  $permissions['delete own accolade content'] = array(
    'name' => 'delete own accolade content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any accolade content'.
  $permissions['edit any accolade content'] = array(
    'name' => 'edit any accolade content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_accolade_type'.
  $permissions['edit field_accolade_type'] = array(
    'name' => 'edit field_accolade_type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_accolade_type_other'.
  $permissions['edit field_accolade_type_other'] = array(
    'name' => 'edit field_accolade_type_other',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_introduction_text'.
  $permissions['edit field_introduction_text'] = array(
    'name' => 'edit field_introduction_text',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_status'.
  $permissions['edit field_status'] = array(
    'name' => 'edit field_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_accolade_type'.
  $permissions['edit own field_accolade_type'] = array(
    'name' => 'edit own field_accolade_type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_accolade_type_other'.
  $permissions['edit own field_accolade_type_other'] = array(
    'name' => 'edit own field_accolade_type_other',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_introduction_text'.
  $permissions['edit own field_introduction_text'] = array(
    'name' => 'edit own field_introduction_text',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_status'.
  $permissions['edit own field_status'] = array(
    'name' => 'edit own field_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own accolade content'.
  $permissions['edit own accolade content'] = array(
    'name' => 'edit own accolade content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter accolade revision log entry'.
  $permissions['enter accolade revision log entry'] = array(
    'name' => 'enter accolade revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override accolade authored by option'.
  $permissions['override accolade authored by option'] = array(
    'name' => 'override accolade authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override accolade authored on option'.
  $permissions['override accolade authored on option'] = array(
    'name' => 'override accolade authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override accolade promote to front page option'.
  $permissions['override accolade promote to front page option'] = array(
    'name' => 'override accolade promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override accolade published option'.
  $permissions['override accolade published option'] = array(
    'name' => 'override accolade published option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override accolade revision option'.
  $permissions['override accolade revision option'] = array(
    'name' => 'override accolade revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override accolade sticky option'.
  $permissions['override accolade sticky option'] = array(
    'name' => 'override accolade sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'view field_accolade_type'.
  $permissions['view field_accolade_type'] = array(
    'name' => 'view field_accolade_type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_accolade_type_other'.
  $permissions['view field_accolade_type_other'] = array(
    'name' => 'view field_accolade_type_other',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_introduction_text'.
  $permissions['view field_introduction_text'] = array(
    'name' => 'view field_introduction_text',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_status'.
  $permissions['view field_status'] = array(
    'name' => 'view field_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_accolade_type'.
  $permissions['view own field_accolade_type'] = array(
    'name' => 'view own field_accolade_type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_accolade_type_other'.
  $permissions['view own field_accolade_type_other'] = array(
    'name' => 'view own field_accolade_type_other',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_introduction_text'.
  $permissions['view own field_introduction_text'] = array(
    'name' => 'view own field_introduction_text',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_status'.
  $permissions['view own field_status'] = array(
    'name' => 'view own field_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
