<?php

/**
 * @file
 * Template for almost the entire page. Most of the page is inside div#site.
 */

$uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<div class="breakpoint"></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>
    <div class="uw-site--inner">
        <div class="uw-section--inner">
            <div id="skip" class="skip">
                <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
                <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
            </div>
        </div>
        <div id="header" class="uw-header--global">
            <div class="uw-section--inner">
                <?php print render($page['global_header']); ?>
            </div>
        </div>
        <div id="site--offcanvas" class="uw-site--off-canvas <?php print ($uw_theme_branding === 'full') ? 'non_generic_header' : 'generic_header'; ?>">
            <div class="uw-section--inner">
                <?php print render($page['site_header']); ?>
            </div>
        </div>
        <?php if ($uw_theme_branding === 'full') {?>
            <div id="site-colors" class="uw-site--colors">
                <div class="uw-section--inner">
                    <div class="uw-site--cbar">
                        <div class="uw-site--c1 uw-cbar"></div>
                        <div class="uw-site--c2 uw-cbar"></div>
                        <div class="uw-site--c3 uw-cbar"></div>
                        <div class="uw-site--c4 uw-cbar"></div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div id="site-header" class="uw-site--header">
            <div class="uw-section--inner">
                <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
                    <?php print $site_name; ?>
                    <?php if (isset($site_slogan) && $site_slogan !== "") : ?>
                        <span class="uw-site—subtitle"> <?php print $site_slogan; ?> </span>
                    <?php endif; ?>
                </a>
            </div>
        </div>
        <div class="uw-header--banner__alt">
                <div class="uw-section--inner">
                <?php print render($page['banner_alt']); ?>
            </div><!-- /uw-headeranner__alt -->
        </div>
        <div id="banner-area" class="uw-site--banner">
            <div class="uw-section--inner">
                <div class="banner-item">
                    <img id="banner-image" src="/president/sites/ca.president/files/uploads/images/thumbsup.jpg" alt="Image for accolades landing page">
                    <div class="banner-text">
                        <div class="highlight">
                            <h1>PRESIDENT’S ACCOLADES</h1>
                            <p class="lead">Recognizing those who go Above and Beyond</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="main" class="uw-site--main" role="main">
            <div class="uw-section--inner">
                <div class="uw-site--main-top">
                    <div class="uw-site--banner">
                        <?php print render($page['banner']); ?>
                    </div>
                    <div class="uw-site--messages">
                        <?php print $messages; ?>
                    </div>
                    <div class="uw-site--help">
                        <?php print render($page['help']); ?>
                    </div>
                    <div class="uw-site--breadcrumb">
                        <?php print $breadcrumb; ?>
                    </div>
                    <div class="uw-site--title">
                        <?php list($title, $content) = uw_fdsu_theme_separate_h1_and_content($page, $title); ?>
                        <h1><?php print $title ?: $site_name; ?></h1>
                    </div>
                </div>
                <!-- when logged in -->
                <?php if ($tabs) : ?>
                    <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div>
                <?php endif; ?>
                <?php if ($action_links): ?>
                    <ul class="action-links"><?php print render($action_links); ?></ul>
                <?php endif; ?>
                <div class="uw-site-main--content accolades">
                    <div id="content" class="uw-site-content">
                        <?php print $content; ?>
                    </div><!--/main-content-->
                    <?php $sidebar = render($page['sidebar_second']); ?>
                    <?php $sidebar_promo = render($page['promo']); ?>
                    <?php if (isset($sidebar) || isset($sidebar_promo)) { ?>
                        <?php if (($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '')) { ?>
                            <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper">
                                <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                                    <div id="site-sidebar" class="uw-site-sidebar<?php echo ($sidebar_promo !== NULL && $sidebar_promo !== '') ? ' sticky-promo' : ''; ?>">
                                        <?php if (isset($sidebar_promo)) { ?>
                                            <?php if ($sidebar_promo !== NULL && $sidebar_promo !== '') { ?>
                                                <div class="uw-site-sidebar--promo">
                                                    <?php print render($page['promo']); ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <div class="uw-site-sidebar--second">
                                            <?php print render($page['sidebar_second']); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div><!--/section inner-->
        </div><!--/site main-->
        <?php if ($uw_theme_branding !== 'generic_wordmark' && $uw_theme_branding !== 'generic_barebones') { ?>
            <div class="uw-site--watermark">
                <div class="uw-section--inner"></div>
            </div>
        <?php } ?>
        <div id="footer" class="uw-footer" role="contentinfo">
            <div id="uw-site-share" class="uw-site-share">
                <div class="uw-section--inner">
                    <div class="uw-section-share"></div>
                    <ul class="uw-site-share-top">
                        <li class="uw-site-share--button__top">
                            <a href="#main" id="uw-top-button" class="uw-top-button">
                                <span class="ifdsu fdsu-arrow"></span>
                                <span class="uw-footer-top-word">TOP</span>
                            </a>
                        </li>
                        <li class="uw-site-share--button__share">
                            <a href="#uw-site-share" class="uw-footer-social-button">
                                <span class="ifdsu fdsu-share"></span>
                                <span class="uw-footer-share-word">Share</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="site-footer" class="uw-site-footer site-footer-toggle open-site-footer">
                <div class="uw-section--inner">
                    <div class="uw-site-footer1<?php print empty($page['site_footer']) ? ' uw-no-site-footer' : ''; ?>">
                        <div class="uw-site-footer1--logo-dept">
                            <?php
                            $site_logo = variable_get('uw_nav_site_footer_logo');
                            $site_logo_link = variable_get('uw_nav_site_footer_logo_link');
                            $facebook = variable_get('uw_nav_site_footer_facebook');
                            $twitter = variable_get('uw_nav_site_footer_twitter');
                            $instagram = variable_get('uw_nav_site_footer_instagram');
                            $youtube = variable_get('uw_nav_site_footer_youtube');
                            $linkedin = variable_get('uw_nav_site_footer_linkedin');
                            $snapchat = variable_get('uw_nav_site_footer_snapchat');

                            $alt_tag = str_replace('_', ' ', $site_logo) . ' logo';
                            $arr = explode(" ", $alt_tag);
                            $arr = array_unique($arr);
                            $alt_tag = implode(" ", $arr);
                            ?>
                            <?php if ($site_logo_link) : ?>
                                <a href="<?php print $site_logo_link; ?>"><img src="<?php print base_path() . drupal_get_path('module', 'uw_nav_site_footer'); ?>/logos/<?php print $site_logo; ?>.png" alt="<?php print $alt_tag; ?>" /></a>
                            <?php else : ?>
                                <?php $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>'; ?>
                            <?php endif; ?>
                        </div>
                        <div class="uw-site-footer1--contact">
                            <ul class="uw-footer-social">
                                <?php if ((string) $facebook !== ''): ?>
                                    <li>
                                        <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>" aria-label="facebook">
                                            <span class="ifdsu fdsu-facebook"></span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if ((string) $twitter !== ''): ?>
                                    <li>
                                        <a href="https://www.twitter.com/<?php print check_plain($twitter); ?>" aria-label="twitter">
                                            <span class="ifdsu fdsu-twitter"></span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if ((string) $youtube !== ''): ?>
                                    <li>
                                        <a href="<?php print check_plain($youtube); ?>" aria-label="youtube">
                                            <span class="ifdsu fdsu-youtube"></span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if ((string) $instagram !== ''): ?>
                                    <li>
                                        <a href="<?php print check_plain($instagram); ?>" aria-label="instagram">
                                            <span class="ifdsu fdsu-instagram"></span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if ((string) $linkedin !== ''): ?>
                                    <li>
                                        <a href="<?php check_plain(print $linkedin); ?>" aria-label="linkedin">
                                            <span class="ifdsu fdsu-linkedin"></span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if ((string) $snapchat !== ''): ?>
                                    <li>
                                        <a href="https://www.snapchat.com/add/<?php check_plain(print $snapchat); ?>" aria-label="snapchat">
                                            <span class="ifdsu fdsu-snapchat"></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <?php if (!empty($page['site_footer']['uw_nav_site_footer_site-footer']['#node']->field_body_no_summary)): ?>
                        <div class="uw-site-footer2">
                            <?php print render($page['site_footer']); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php print render($page['global_footer']); ?>
        </div>
    </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
