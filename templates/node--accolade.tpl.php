<?php

/**
 * @file
 */
?>


<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
    <div class="content_node"<?php print $content_attributes; ?>>
        <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);


        hide($content['links']);
        // Hide things that we are manually rendering.
        hide($content['field_fullname']);
        hide($content['field_link_url_kudo']);
        hide($content['field_accolade_description']);
        print '<a class="accolade-link" href="' . render($content['field_link_url_accolade']['#items'][0]['url']) . '">';
        print '<div class="accolade">';

        print '<div class="accolade-image">' . render($content['field_photo']) . '</div>';
        print '<h3 class="accolade-title">' . $title . '</h3>';
        print '<p class="accolade-info">' . render($content['field_accolade_description'][0]['#markup']) . '</p>';
        print '</div>';
        print '</a>';
//        hide($content['addtoany']);

        print '<div class="accolade-date">' . date('M \'y', $node->created) . '</div>';
        print '<div class="accolade-addtoany">';
        print render($content['addtoany']);
        print '</div>';
        ?>
    </div>

</div> <!-- /node-->
