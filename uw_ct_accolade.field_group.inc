<?php
/**
 * @file
 * uw_ct_accolade.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_accolade_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_approval|node|accolade|form';
  $field_group->group_name = 'group_approval';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'accolade';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Approval and Release',
    'weight' => '4',
    'children' => array(
      0 => 'field_permissions',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_approval|node|accolade|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_personal_information|node|accolade|form';
  $field_group->group_name = 'group_personal_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'accolade';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Personal Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_email_address',
      1 => 'field_fullname',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Personal Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'You are not required to include your last name.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_personal_information|node|accolade|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo|node|accolade|form';
  $field_group->group_name = 'group_photo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'accolade';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Your Photo',
    'weight' => '3',
    'children' => array(
      0 => 'field_photo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_photo|node|accolade|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_questions|node|accolade|form';
  $field_group->group_name = 'group_profile_questions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'accolade';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'accolade Information',
    'weight' => '2',
    'children' => array(
      0 => 'field_accolade_description',
      1 => 'field_accolade_type',
      2 => 'field_accolade_type_other',
      3 => 'field_link_url_accolade',
      4 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_profile_questions|node|accolade|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_status|node|accolade|form';
  $field_group->group_name = 'group_status';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'accolade';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'accolade Status/Actions',
    'weight' => '5',
    'children' => array(
      0 => 'field_status',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_status|node|accolade|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Approval and Release');
  t('accolade Information');
  t('accolade Status/Actions');
  t('Personal Information');
  t('Upload Your Photo');

  return $field_groups;
}
