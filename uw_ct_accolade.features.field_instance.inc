<?php
/**
 * @file
 * uw_ct_accolade.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_ct_accolade_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-accolade-field_email_address'.
  $field_instances['node-accolade-field_email_address'] = array(
    'bundle' => 'accolade',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Your email will not appear on the website.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_email_address',
    'label' => 'Email Address',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-accolade-field_fullname'.
  $field_instances['node-accolade-field_fullname'] = array(
    'bundle' => 'accolade',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_fullname',
    'label' => 'Name',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'single_page_remote_events' => 'single_page_remote_events',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-accolade-field_accolade_description'.
  $field_instances['node-accolade-field_accolade_description'] = array(
    'bundle' => 'accolade',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_accolade_description',
    'label' => 'Tell us about the project, person, or new piece',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'single_page_remote_events' => 'single_page_remote_events',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-accolade-field_accolade_type'.
  $field_instances['node-accolade-field_accolade_type'] = array(
    'bundle' => 'accolade',
    'default_value' => array(
      0 => array(
        'value' => 'accolade',
      ),
    ),
    'deleted' => 0,
    'description' => '<p>Check <strong>all</strong> that apply.</p>',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_accolade_type',
    'label' => 'Are you a(n):',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_buttons',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-accolade-field_accolade_type_other'.
  $field_instances['node-accolade-field_accolade_type_other'] = array(
    'bundle' => 'accolade',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_accolade_type_other',
    'label' => 'Other Type:',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'single_page_remote_events' => 'single_page_remote_events',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-accolade-field_introduction_text'.
  $field_instances['node-accolade-field_introduction_text'] = array(
    'bundle' => 'accolade',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_introduction_text',
    'label' => 'Introduction text',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'markup',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-accolade-field_link_url_accolade'.
  $field_instances['node-accolade-field_link_url_accolade'] = array(
    'bundle' => 'accolade',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 4,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_link_url_accolade',
    'label' => 'Link / URL to accolade',
    'required' => 1,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'entity_translation_sync' => FALSE,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_allowed_values' => '',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'link_field',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-accolade-field_permissions'.
  $field_instances['node-accolade-field_permissions'] = array(
    'bundle' => 'accolade',
    'default_value' => array(
      0 => array(
        'value' => 'Online (websites)',
      ),
      1 => array(
        'value' => 'Internal publications',
      ),
    ),
    'deleted' => 0,
    'description' => '<p>Uncheck boxes if you do not want your information used in this manner.</p>
<p>I understand that my information will be retained by the University of Waterloo, and may be used in other publications produced by the University. I waive any compensation from or recourse against the University of Waterloo for the use of my photographs and quotations.</p>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_permissions',
    'label' => 'I hereby give my permission to the University of Waterloo to publish my photograph and quotations for:',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_buttons',
      'weight' => 18,
    ),
  );

  // Exported field_instance: 'node-accolade-field_photo'.
  $field_instances['node-accolade-field_photo'] = array(
    'bundle' => 'accolade',
    'deleted' => 0,
    'description' => '<p>Your photo must be:</p>
<ul>

<li>in png, gif, jpg, jpeg format</li>
</ul>',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'international_accolades',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_photo',
    'label' => 'Upload a photo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'alt_field_required' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'accolade_photos',
      'file_extensions' => 'png gif jpg jpeg',
      'focal_point' => 0,
      'focal_point_preview' => 0,
      'focal_point_styles' => array(
        'image_gallery_small' => 0,
        'image_gallery_squares' => 0,
        'image_gallery_standard' => 0,
        'image_gallery_wide' => 0,
      ),
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'uw_tf_standard',
          'value' => '',
        ),
      ),
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '1024x768',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'copy',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__banner-750w' => 0,
          'colorbox__banner-wide' => 0,
          'colorbox__body-500px-wide' => 0,
          'colorbox__field-slideshow-slide' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__image_gallery_small' => 0,
          'colorbox__image_gallery_squares' => 0,
          'colorbox__image_gallery_standard' => 0,
          'colorbox__image_gallery_wide' => 0,
          'colorbox__international_accolades' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__person-profile-list' => 0,
          'colorbox__profile-photo-block' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__uw_service_icon' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_banner-750w' => 0,
          'image_banner-wide' => 0,
          'image_body-500px-wide' => 0,
          'image_field-slideshow-slide' => 0,
          'image_focal_point_preview' => 0,
          'image_image_gallery_small' => 0,
          'image_image_gallery_squares' => 0,
          'image_image_gallery_standard' => 0,
          'image_image_gallery_wide' => 0,
          'image_international_accolades' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_uw_service_icon' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'label_help_description' => '',
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-accolade-field_status'.
  $field_instances['node-accolade-field_status'] = array(
    'bundle' => 'accolade',
    'default_value' => array(
      0 => array(
        'value' => 'decision pending',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_status',
    'label' => 'Status',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 20,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('<p>Check <strong>all</strong> that apply.</p>');
  t('<p>Uncheck boxes if you do not want your information used in this manner.</p>
<p>I understand that my information will be retained by the University of Waterloo, and may be used in other publications produced by the University. I waive any compensation from or recourse against the University of Waterloo for the use of my photographs and quotations.</p>');
  t('<p>Your photo must be:</p>
<ul>

<li>in png, gif, jpg, jpeg format</li>
</ul>');
  t('Are you a(n):');
  t('Email Address');
  t('I hereby give my permission to the University of Waterloo to publish my photograph and quotations for:');
  t('Introduction text');
  t('Link / URL to accolade');
  t('Name');
  t('Other Type:');
  t('Status');
  t('Tell us about the project, person, or new piece');
  t('Upload a photo');
  t('Your email will not appear on the website.');

  return $field_instances;
}
