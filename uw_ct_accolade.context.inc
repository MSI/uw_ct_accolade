<?php
/**
 * @file
 * uw_ct_accolade.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_accolade_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_accolades_homepage';
  $context->description = 'Display a random accolade on the home page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-accolades-uw_accolade_block' => array(
          'module' => 'views',
          'delta' => 'accolades-uw_accolade_block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display a random accolade on the home page');
  $export['uw_accolades_homepage'] = $context;

  return $export;
}
